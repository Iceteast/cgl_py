#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import os , time, random
import cfg

LIVE	= '\033[93m■\033[97m'
DEAD	= '\033[96m■\033[97m'

class Cell:

	def __init__(self, locate, symbol):
		self.symbol = symbol
		self.locate = locate
		self.change = ''

	def getLocation(self):

		return self.locate

	def getSymbol(self):

		return self.symbol

	def mark(self, symbol):
		self.change = symbol

	def isMarked(self):
		return self.change != ""

	def getColor(self):

		if self.symbol == LIVE:	#这里的颜色有问题....
			return cfg.COLOR_LIVE
		else:
			return cfg.COLOR_DEAD

	def setSymbol(self):
		if self.change != '':
			self.symbol = self.change
			self.change = ''

class Conway:

	def __init__(self, width, height):
		self.xMount = width
		self.yMount = height
		self.cells	= []
		self.initChess()

	def initChess(self):
		self.cells = [[Cell((i, j), LIVE) if random.random() < cfg.LIFE_DENSITY else Cell((i, j), DEAD) for i in range(self.xMount)]
					for j in range(self.yMount)]
		# self.cells = [[Cell((i, j), DEAD) for i in range(self.xMount)]
		# 			for j in range(self.yMount)]
		# self.cells[10][10].mark(LIVE)
		# self.cells[10][10].setSymbol()
		# self.cells[10][11].mark(LIVE)
		# self.cells[10][11].setSymbol()
		# self.cells[10][12].mark(LIVE)
		# self.cells[10][12].setSymbol()
		# self.cells[11][12].mark(LIVE)
		# self.cells[11][12].setSymbol()
		# self.cells[12][11].mark(LIVE)
		# self.cells[12][11].setSymbol()

	def getX(self):
		return self.xMount

	def getY(self):
		return self.yMount

	def getCell(self, i, j):
		if 0 < i < self.xMount and 0 < j < self.yMount:
			return self.cells[j][i]
		else:
			return Cell((i, j), DEAD)

	def getEnv(self, i, j):
		ans = []
		count = 0
		ans.append(self.getCell(i - 1, j - 1).getSymbol())
		ans.append(self.getCell(i - 0, j - 1).getSymbol())
		ans.append(self.getCell(i + 1, j - 1).getSymbol())
		ans.append(self.getCell(i - 1, j - 0).getSymbol())
		ans.append(self.getCell(i + 1, j - 0).getSymbol())
		ans.append(self.getCell(i - 1, j + 1).getSymbol())
		ans.append(self.getCell(i - 0, j + 1).getSymbol())
		ans.append(self.getCell(i + 1, j + 1).getSymbol())
		for k in ans:
			if k == LIVE:
				count += 1
		return count

	def step(self):
		for j in range(self.yMount):
			for i in range(self.xMount):
				ans = self.getEnv(i, j)
				stet = self.getCell(i, j).getSymbol()
				if stet == LIVE:
					if ans < 2 or ans > 3:
						self.cells[j][i].mark(DEAD)
				else:
					if ans == 3:
						self.cells[j][i].mark(LIVE)

		for j in range(self.yMount):
			for i in range(self.xMount):
				if self.cells[j][i].isMarked():
					self.cells[j][i].setSymbol()

	def show(self):
		print('\n'.join(' '.join(self.cells[j][i].getSymbol() for i in range(self.xMount))
					for j in range(self.yMount)))


if __name__ == "__main__":
	w = 20
	h = 20
	s = 2
	world = Conway(w, h)
	world.show()
	while s > 0:
		time.sleep(0.75)
		s -= 1
		os.system('clear')
		world.step()
		world.show()