#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import os , time, sys
import pygame , cfg

from ConwayLife import Conway
from pygame.locals import *

def drawMap(game, screen):

	for j in range(game.getY()):
		for i in range(game.getX()):
			pygame.draw.rect(screen, game.getCell(i, j).getColor(), Rect(cfg.BLANK_SIZE + j * (cfg.BLOCK_SIZE + cfg.BLANK_SIZE), cfg.BLANK_SIZE + i * (cfg.BLOCK_SIZE + cfg.BLANK_SIZE), cfg.BLOCK_SIZE, cfg.BLOCK_SIZE))


pygame.init()
screen = pygame.display.set_mode(cfg.SCREEN_SIZE)
pygame.display.set_caption("Conway's Game of Life")

game_over = False
game = Conway(cfg.SCREEN_SIZE[0] // (cfg.BLOCK_SIZE + cfg.BLANK_SIZE),
					cfg.SCREEN_SIZE[1] // (cfg.BLOCK_SIZE + cfg.BLANK_SIZE))

while not game_over:

	for event in pygame.event.get():
		if event.type == QUIT:
			sys.exit()

	keys = pygame.key.get_pressed()
	if keys[K_ESCAPE]:
		sys.exit()
	elif keys[K_r]:
		game_over = True

	screen.fill(cfg.COLOR_BACKGROUND)
	drawMap(game, screen)
	game.step()
	time.sleep(cfg.GAME_SPEED)
	pygame.display.update()
